package com.ultra.mega;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class FirstActivity extends AppCompatActivity {
    DatabaseHelper db;
    Button logout;
    TextView userName,userEmail,userId;
    ImageView profileImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
//        String username = getIntent().getStringExtra("Username");
//        String personname = getIntent().getStringExtra("personName");
        db   = new DatabaseHelper(this);
        Cursor cursor = db.alldata();

        userEmail=findViewById(R.id.email);
//        userEmail.setText(username);
        userId=findViewById(R.id.userId);
//        userId.setText(personname);
        if (cursor.getCount()== 0){
            Toast.makeText(getApplicationContext(), "No Data", Toast.LENGTH_SHORT).show();
        }else
        {
            while (cursor.moveToNext()){
                userId.setText(cursor.getString(1));
                userEmail.setText(cursor.getString(2));
            }
        }

        logout = findViewById(R.id.btn_logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(
//                        new ResultCallback<Status>() {
//                            @Override
//                            public void onResult(Status status) {
//                                Toast.makeText(getApplicationContext(), "Google Logout is successful", Toast.LENGTH_SHORT).show();
//
//                            }
//                        });
                Toast.makeText(getApplicationContext(), "Logout is successful", Toast.LENGTH_SHORT).show();
                Intent loginIntent = new Intent(FirstActivity.this, LoginActivity.class);
                startActivity(loginIntent);
                finish();


            }
        });

    }
}
